const { comment } = require("postcss");

const allowCors = (fn) => async (req, res) => {
  res.setHeader("Access-Control-Allow-Credentials", true);
  res.setHeader("Access-Control-Allow-Origin", "*");
  // another common pattern
  // res.setHeader('Access-Control-Allow-Origin', req.headers.origin);
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,OPTIONS,PATCH,DELETE,POST,PUT"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version"
  );
  if (req.method === "OPTIONS") {
    res.status(200).end();
    return;
  }

  return await fn(req, res);
};

async function retrieveComments(videoId, apiKey, pageToken = "", array = []) {
  const apiUrl = `https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&videoId=${videoId}&maxResults=100&key=${apiKey}&pageToken=${pageToken}`;

  try {
    const response = await fetch(apiUrl);
    const data = await response.json();
    const comments = data.items.map(
      (item) => item.snippet.topLevelComment.snippet.authorDisplayName
    );

    if (data.nextPageToken) {
      return await retrieveComments(videoId, apiKey, data.nextPageToken, [
        ...array,
        ...comments,
      ]);
    } else {
      return [...array, ...comments];
    }
  } catch (error) {
    console.error("Error retrieving comments:", error);
  }
}

const handler = (req, res) => {
  const videoId = req.query.videoId;
  const apiKey = process.env.API_KEY;
  retrieveComments(videoId, apiKey)
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((error) => {
      console.error("Error:", error);
      res.status(404);
    });
};

module.exports = allowCors(handler);
